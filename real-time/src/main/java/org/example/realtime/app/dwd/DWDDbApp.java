package org.example.realtime.app.dwd;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.example.app.BaseApp;

/**
 * @ClassName DWDDbApp
 * @Description
 * @Author QinQiuFu
 * @Date 2021/4/22 8:36
 */
public class DWDDbApp extends BaseApp {
    public static void main(String[] args) {
        new DWDDbApp().init(2000,
                "DWDDbApp",
                "ods_db",
                "DWDDbApp");

    }

    @Override
    public void run(DataStreamSource<String> streamSource, StreamTableEnvironment tenv) {
//           根据 tableProcess  将ods_log中的数据  每张表写入到kafka 或者hbase
//          获取tableprocess 数据流 做成广播流 connect实时判断每张表的属性和去向（kafka，hbase
        tenv.executeSql("create table tableProcess()with()");
        Table tableProcess = tenv.sqlQuery("select * from tableProcess");
        DataStream<Tuple2<Boolean, org.example.realtime.bean.TableProcess>> tableProcessStream = tenv.toRetractStream(tableProcess, org.example.realtime.bean.TableProcess.class);

    }
}
