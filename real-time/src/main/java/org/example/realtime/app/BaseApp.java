package org.example.realtime.app;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.example.realtime.util.MyKafkaUtils;

/**
 * @ClassName BaseApp
 * @Description
 * @Author QinQiuFu
 * @Date 2021/4/22 8:41
 */
public abstract class BaseApp {
    public void init(int port, String checkPointDir, String topic, String group) {
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        Configuration conf = new Configuration();
        conf.setInteger("rest.port", port);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment(conf);
        env.setParallelism(2);
        env.setStateBackend(new FsStateBackend("hdfs://hadoop162:8020/flink-realtime02/" + checkPointDir));
        env.enableCheckpointing(1000 * 20, CheckpointingMode.EXACTLY_ONCE);
        env.getCheckpointConfig().setCheckpointTimeout(60000);

        env.getCheckpointConfig()
                .enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        DataStreamSource<String> streamSource = env.addSource(MyKafkaUtils.getKafkaSource(topic, group));
        StreamTableEnvironment tenv = StreamTableEnvironment.create(env);
        run(streamSource, tenv);

    }

    public abstract void run(DataStreamSource<String> streamSource, StreamTableEnvironment tenv);
}
