package org.example.realtime.util;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;

import java.util.Properties;

/**
 * @ClassName MyKafkaUtils
 * @Description
 * @Author QinQiuFu
 * @Date 2021/4/22 8:54
 */
public class MyKafkaUtils {
    public static FlinkKafkaConsumer<String> getKafkaSource(String topic, String group) {
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "hadoop162:9092,hadoop63:9092,hadoop164:9092");
        props.setProperty("group.id", group);
        props.setProperty("auto.offset.reset", "latest");
        return new FlinkKafkaConsumer<String>(topic, new SimpleStringSchema(), props);
    }
}